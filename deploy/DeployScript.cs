﻿using FlubuCore.Context;
using FlubuCore.Scripting;

namespace DeployScript
{
    public class DeployScript : DefaultBuildScript
    {
        private string Project => "../src/ProjectXignal.Xf.Ooui/ProjectXignal.Xf.Ooui.csproj";
        private string Runtime => "linux-arm";

        [FromArg("pi", "hostname or ip-address of rasperry pi")]
        public string HostPi { get; set; } =  "motilePi";

        protected override void ConfigureBuildProperties(IBuildPropertiesContext context)
        {
            // context.Properties.Set(BuildProps.ProductId, "FlubuExample");
            // context.Properties.Set(BuildProps.ProductName, "FlubuExample");
            //context.Properties.Set(BuildProps.SolutionFileName, Project);
            //context.Properties.Set(BuildProps.BuildConfiguration, "Release");

        }

        protected override void ConfigureTargets(ITaskContext session)
        {

            System.Console.WriteLine(HostPi);
            
            session.CreateTarget("deploy")               
                .DependsOn("clean")
                .DependsOn("restore")
                .DependsOn("publish")
                .DependsOn("deployPi")
                 .DependsOn("configurePi")
                .SetAsDefault();

            session.CreateTarget("clean")
                .AddTask(x => x.CreateDirectoryTask("../artifacts", true));

            session.CreateTarget("restore")
                .AddTask(x => x.GitTasks().InitSubmodules())
                .AddCoreTask(x => x.Restore(Project).AddRuntime(Runtime));

            session.CreateTarget("publish")
                .AddCoreTask(x => x.Publish(Project)
                    .OutputDirectory("../artifacts/publish")
                    .SelfContained()
                    .AddRuntime(Runtime))
                .AddTask(x => x.PackageTask(@"../artifacts")
                    .AddDirectoryToPackage("../artifacts/publish", "", true)
                    .AddFileToPackage("../lib/ws2811.so", "")
                    .AddFileToPackage("ProjectXignal.service", "")
                    .ZipPackage("build.zip", false));

            // .AddTask(x => x.Chocolatey().Install("putty").WithArguments("-y"))

            session.CreateTarget("deployPi")
                .AddCoreTask(x => x.LinuxTasks()
                .SshCommand(HostPi, "pi", "motile")
                    .WithCommand(Stop)
                    .WithCommand("rm /home/pi/ProjectXignal/*"))
                .AddTask(x => x.RunProgramTask("pscp")
                    .WorkingFolder(".")
                    .WithArguments("-pw", "motile")
                    .WithArguments(@"../artifacts/build.zip")
                    .WithArguments($"pi@{HostPi}:/home/pi/ProjectXignal"))
                .AddCoreTask(x => x.LinuxTasks().SshCommand(HostPi, "pi", "motile")                    
                    .WithCommand("unzip /home/pi/ProjectXignal/build.zip -d /home/pi/ProjectXignal")
                    .WithCommand("rm /home/pi/ProjectXignal/build.zip"));

            session.CreateTarget("configurePi")
                .AddCoreTask(x => x.LinuxTasks()
                .SshCommand(HostPi, "pi", "motile")

                    .WithCommand("sudo chmod +x /home/pi/ProjectXignal/ProjectXignal.Xf.Ooui")
                    .WithCommand("sudo cp -f /home/pi/ProjectXignal/ws2811.so /usr/lib/ws2811.so")
                    .WithCommand("sudo cp -f /home/pi/ProjectXignal/ProjectXignal.service /lib/systemd/system/ProjectXignal.service")
                    .WithCommand("sudo systemctl daemon-reload")
                    .WithCommand(Start)
                    .WithCommand("sudo systemctl enable ProjectXignal")
            )


                //.AddCoreTask(x => x.LinuxTasks()
                //.SshCopy(Host, "pi", "motile")
                //.WithFile(@"c:\temp\build.zip", ".")



                //)

                ;
        }

        private string Stop => "systemctl -q is-active ProjectXignal.service\n if [ $? -eq 0 ]; then\n sudo systemctl stop ProjectXignal \n echo 'stop ProjectXignal'\n exit 0\n fi";

        private string Start => "sudo systemctl start ProjectXignal";

    }
}
