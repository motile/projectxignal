﻿using ProjectXignal.Xf.Pi.Behaviors;
using ProjectXignal.Xf.Controls;
using Xamarin.Forms;

namespace ProjectXignal.Xf
{
    public static class AppExtensions
    {
        public static void AddPi(this ProjectXignal.Xf.App app)
        {
            if (Iot.Device.PiHelper.IsPi())
            {
                app.AddPiUnicorn();
                app.AddPiDrumhat();
            }
        }

        private static void AddPiUnicorn(this ProjectXignal.Xf.App app)
        {
            var style = new Style(typeof(UserStatesControl));
            style.Setters.Add(new Setter
            {
                Property = UserStatesUnicornBehavior.AttachBehaviorProperty,
                Value = true
            });

            app.Resources.Add(style);
        }

        private static void AddPiDrumhat(this ProjectXignal.Xf.App app)
        {
            var style = new Style(typeof(CommandBlockControl));
            style.Setters.Add(new Setter
            {
                Property = CommandDrumhatBehavior.AttachBehaviorProperty,
                Value = true
            });

            app.Resources.Add(style);
        }
    }
}
