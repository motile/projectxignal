﻿using rpi_ws281x;
using System.Drawing;

namespace ProjectXignal.Xf.Pi
{
    public class UnicornHat
    {
        private static UnicornHat _singleton;
        private static object _lock = new object();
        private readonly Controller _controller;
        private readonly WS281x _device;

        public static UnicornHat Instance
        {
            get
            {
                if (_singleton == null)
                {
                    lock (_lock)
                    {
                        if (_singleton == null)
                        {
                            _singleton = new UnicornHat();
                        }
                    }
                }
                return _singleton;
            }
        }

        private UnicornHat()
        {
            var settings = Settings.CreateDefaultSettings();

            _controller = settings.AddController(32, Pin.Gpio18, StripType.WS2811_STRIP_GRB, brightness: 70);

            _device = new WS281x(settings);
        }

        ~UnicornHat()
        {
            _device.Dispose();
        }

        public void All(Color color)
        {   
            _controller.SetAll(color);
            _device.Render();
        }

        public void Draw(Color[] colors)
        {   
            _device.Reset();

            bool[,] m = AllOn;

            int width = 8 / colors.Length;

            for (var i = 0; i < colors.Length; i++)
            {
                var color = colors[i];

                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < 4; y++)
                    {
                        if (m[x, y])
                        {
                            var index = GetIndex(Transform((x, y), i, width));

                            _controller.SetLED(index, color);
                        }
                    }
                }
            }

            _device.Render();
        }

        private bool[,] AllOn = new bool[4, 4] {
            { true, true, true, true },
            { true, true, true, true },
            { true, true, true, true },
            { true, true, true, true }
        };

        private (int x, int y) Transform((int x, int y) pos, int c, int width)
        {
            return ((c) * width + pos.x, pos.y);
        }

        private int GetIndex((int x, int y) pos)
        {
            return (pos.y) * 8 + pos.x;
        }
    }
}
