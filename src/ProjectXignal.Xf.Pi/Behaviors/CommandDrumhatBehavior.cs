﻿using ProjectXignal.Xf.Controls;
using System.Device;
using System.Linq;
using Xamarin.Forms;

namespace ProjectXignal.Xf.Pi.Behaviors
{
    public class CommandDrumhatBehavior : Behavior<CommandBlockControl>
    {
        public static readonly BindableProperty AttachBehaviorProperty =
                BindableProperty.CreateAttached("AttachBehavior", typeof(bool), typeof(CommandDrumhatBehavior), false, propertyChanged: OnAttachBehaviorChanged);

        public static bool GetAttachBehavior(BindableObject view)
        {
            return (bool)view.GetValue(AttachBehaviorProperty);
        }

        public static void SetAttachBehavior(BindableObject view, bool value)
        {
            view.SetValue(AttachBehaviorProperty, value);
        }

        static void OnAttachBehaviorChanged(BindableObject view, object oldValue, object newValue)
        {
            if (!(view is CommandBlockControl entry))
            {
                return;
            }

            bool attachBehavior = (bool)newValue;
            if (attachBehavior)
            {
                entry.Behaviors.Add(new CommandDrumhatBehavior());
            }
            else
            {
                var toRemove = entry.Behaviors.FirstOrDefault(b => b is CommandDrumhatBehavior);
                if (toRemove != null)
                {
                    entry.Behaviors.Remove(toRemove);
                }
            }
        }

        protected override void OnAttachedTo(CommandBlockControl bindable)
        {
            DrumHat.Instance.Pressed += (a, b) =>
            {
                var v = (int)b.Pad;
                var p = bindable.Parameters?.Cast<object>().ToArray();
                if (v < p?.Length)
                {
                    bindable.Command.Execute(p[v]);
                }
            };

            base.OnAttachedTo(bindable);
        }

    }
}
