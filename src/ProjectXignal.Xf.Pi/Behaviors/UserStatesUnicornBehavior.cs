﻿using ProjectXignal.Xf.Controls;
using ProjectXignal.Xf.Converters;
using System.Linq;
using Xamarin.Forms;

namespace ProjectXignal.Xf.Pi.Behaviors
{
    public class UserStatesUnicornBehavior : Behavior<UserStatesControl>
    {
        public static readonly BindableProperty AttachBehaviorProperty =
        BindableProperty.CreateAttached("AttachBehavior", typeof(bool), typeof(UserStatesUnicornBehavior), false, propertyChanged: OnAttachBehaviorChanged);

        public static bool GetAttachBehavior(BindableObject view)
        {
            return (bool)view.GetValue(AttachBehaviorProperty);
        }

        public static void SetAttachBehavior(BindableObject view, bool value)
        {
            view.SetValue(AttachBehaviorProperty, value);
        }

        static void OnAttachBehaviorChanged(BindableObject view, object oldValue, object newValue)
        {
            if (!(view is UserStatesControl entry))
            {
                return;
            }

            bool attachBehavior = (bool)newValue;
            if (attachBehavior)
            {
                entry.Behaviors.Add(new UserStatesUnicornBehavior());
            }
            else
            {
                var toRemove = entry.Behaviors.FirstOrDefault(b => b is UserStatesUnicornBehavior);
                if (toRemove != null)
                {
                    entry.Behaviors.Remove(toRemove);
                }
            }
        }

        protected override void OnAttachedTo(UserStatesControl bindable)
        {
            UnicornHat.Instance.All(System.Drawing.Color.DarkViolet);

            bindable.AfterUserStateChanged += Bindable_OnUserStateChanged;

            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(UserStatesControl bindable)
        {
            bindable.AfterUserStateChanged -= Bindable_OnUserStateChanged;
            UnicornHat.Instance.All(System.Drawing.Color.Black);           

            base.OnDetachingFrom(bindable);
        }

        private void Bindable_OnUserStateChanged(object sender, System.EventArgs e)
        {
            SetUnicorn((UserStatesControl)sender);
        }

        private void SetUnicorn(UserStatesControl control)
        {
            if (control?.UserStates == null)
                return;

            var converter = new UserState2ColorConverter();

            var colors = control.UserStates
                .Select(x => (System.Drawing.Color)converter.Convert(x.State))
                .ToArray();

            UnicornHat.Instance.Draw(colors);
        }

    }
}
