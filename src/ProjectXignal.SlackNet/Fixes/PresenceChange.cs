﻿using SlackNet;
using SlackNet.Events;
using System.Collections.Generic;

namespace ProjectXignal.SlackNet.Fixes
{
    /// <summary>
    /// Replaces <see cref="global::SlackNet.Events.PresenceChange"/>
    /// Users and User respectively may contain data
    /// </summary>
    public class PresenceChange : Event
    {
        public IList<string> Users { get; set; }

        public string User { get; set; }

        public Presence Presence { get; set; }
    }
}
