﻿using SlackNet.Rtm;
using System.Collections.Generic;
using System.Text;

namespace ProjectXignal.SlackNet.Fixes
{
    /// <summary>
    /// Send presence_sub message
    /// could not find it in SlackNet
    /// </summary>
    public class PresenceSub : OutgoingRtmEvent
    {
        public new string Type => "presence_sub";

        public IList<string> Ids { get; set; }
    }
}
