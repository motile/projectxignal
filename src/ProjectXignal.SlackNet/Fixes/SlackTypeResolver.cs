﻿using SlackNet;
using System;

namespace ProjectXignal.SlackNet.Fixes
{
    /// <summary>
    /// Considers <see cref="PresenceChange"/>     
    /// </summary>
    public class FixedSlackTypeResolver : ISlackTypeResolver
    {
        private readonly ISlackTypeResolver _baseResolver = Default.SlackTypeResolver(Default.AssembliesContainingSlackTypes);
        
        public Type FindType(Type baseType, string slackType)
        {
            switch (slackType)
            {
                case "presence_change":
                    return typeof(PresenceChange);
                default:
                    return _baseResolver.FindType(baseType, slackType);
            }

        }
    }
}
