﻿using ProjectXignal.Core;
using ProjectXignal.SlackNet.Fixes;
using SlackNet;
using SlackNet.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Threading.Tasks;

namespace ProjectXignal.SlackNet
{
    public class SlackXignalManager : IXignalManager, IDisposable
    {

        private SlackApiClient _apiClientBot;
        private SlackApiClient _apiClientAccess;
        private SlackRtmClient _rtmClient;
        private readonly string _botToken;
        private readonly string _accessToken;

        public event EventHandler<UserStateChangedEvent> UserStateChanged;

        public SlackXignalManager(string botToken, string accessToken)
        {
            _botToken = botToken;
            _accessToken = accessToken;
        }

        public string User { get; private set; }

        public string UserId { get; private set; }

        public UserState[] UserStates { get; private set; }

        public bool IsConnected => _rtmClient?.Connected ?? false;

        public async Task Connect()
        {
            if (this.IsConnected)
            {
                this.Disconnect();
            }

            this._apiClientBot = new SlackApiClient(_botToken);
            this._apiClientAccess = new SlackApiClient(_accessToken);

            var socketFactory = Default.WebSocketFactory;
            var jsonSettings = Default.JsonSettings(new FixedSlackTypeResolver());
            var scheduler = Scheduler.Default;

            this._rtmClient = new SlackRtmClient(_apiClientBot, socketFactory, jsonSettings, scheduler);

            
            var response = await _rtmClient.Connect(true, true).ConfigureAwait(false);


            var listResponse = await _apiClientBot.Users.List();

            this.UserStates = listResponse.Members
                .Where(x => !string.IsNullOrEmpty(x.Profile.Email))
                .Select(x => new UserState(x.Id, x.Name, State.Unknown))
                .ToArray();

            _rtmClient.Send(new Fixes.PresenceSub() { Ids = UserStates.Select(x => x.Id).ToList() });

            _rtmClient.Events.Subscribe(e =>
            {
                switch (e)
                {
                    case Fixes.PresenceChange pc:
                        var t1 = NotifySubscribers(pc);
                        break;
                    case UserChange uc:
                        var t2 = NotifySubscribers(uc);
                        break;
                }
            });

            var test = await _apiClientAccess.Auth.Test();

            this.User = test.User;
            this.UserId = test.UserId;

        }

        public void Disconnect()
        {
            try
            {
                _rtmClient?.Dispose();
            }
            catch { }

            _rtmClient = null;

            _apiClientBot = null;
        }



        private async Task NotifySubscribers(UserChange userChange)
        {
            var id = userChange.User.Id;

            var info = await _apiClientBot.Users.Info(id);
            var presence = await _apiClientAccess.Users.GetPresence(id);
            
            var user = this.UserStates.Where(x => x.Id == userChange.User.Id).FirstOrDefault();
            if (user == null)
                throw new Exception($"{userChange.User.Id} unknown!");

            user.State = GetState(presence, info?.Profile?.StatusText);
                        
            var uce = new UserStateChangedEvent()
            {
                UserState = new UserState(user.Id, user.Name, user.State)
            };

            this.SendEventToSubscribers(uce);
        }

        private async Task NotifySubscribers(Fixes.PresenceChange pc)
        {
            var ids = pc.Users ?? new List<string>();
            if (!string.IsNullOrEmpty(pc.User))
            {
                ids.Add(pc.User);
            }

            foreach (var id in ids)
            {
                var user = this.UserStates.Where(x => x.Id == id).FirstOrDefault();
                if (user == null)
                    continue;

                var info = await _apiClientBot.Users.Info(id);

                user.State = GetState(pc.Presence, info.Profile.StatusText);

                var uce = new UserStateChangedEvent()
                {
                    UserState = user
                };

                this.SendEventToSubscribers(uce);
            }
        }

        private void SendEventToSubscribers(UserStateChangedEvent uce)
        {
            this.UserStateChanged?.Invoke(this, uce);
        }

        private State GetState(Presence presence, string statusText)
        {
            switch (presence)
            {
                case Presence.Away:
                    return State.Away;
                case Presence.Active:
                    if (string.IsNullOrEmpty(statusText))
                        return State.Ready;
                    break;
            }

            var states = Enum.GetValues(typeof(State)).Cast<State>().ToArray();
            foreach (var s in states)
                if (s.ToString() == statusText)
                    return s;

            return State.Away;
        }

        public async void SetState(UserState userState)
        {
            var (presence, st) = FromState(userState.State);

            var tasks = new Task[] {
                _apiClientAccess.Get("users.setPresence", new Dictionary<string, object> { { "presence", presence } }, null),
                _apiClientAccess.UserProfile.Set(new UserProfile { StatusText = st, StatusEmoji = "" }, userState.Id)
            };

            await Task.WhenAll(tasks);
        }

        private (string presence, string statusText) FromState(State state)
        {
            switch (state)
            {
                case State.Away:
                    return ("away", string.Empty);
                case State.Ready:
                    return ("auto", string.Empty);
                default:
                    return ("auto", $"{state}");
            }
        }

        public void Dispose()
        {
            this.Disconnect();
        }
    }
}
