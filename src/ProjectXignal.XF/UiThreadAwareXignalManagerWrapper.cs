﻿using ProjectXignal.Core;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjectXignal.Xf
{
    public class UiThreadAwareXignalManagerWrapper : IXignalManager
    {
        private readonly IXignalManager _manager;

        public UiThreadAwareXignalManagerWrapper(IXignalManager manager)
        {
            _manager = manager;
            _manager.UserStateChanged += _manager_UserStateChanged;
        }

        public string User => _manager.User;

        public string UserId => _manager.UserId;

        public bool IsConnected => _manager.IsConnected;

        public UserState[] UserStates => _manager.UserStates;

        public event EventHandler<UserStateChangedEvent> UserStateChanged;

        public Task Connect() => _manager.Connect();

        public void Disconnect() => _manager.Disconnect();

        public void SetState(UserState state) => _manager.SetState(state);


        private void _manager_UserStateChanged(object sender, UserStateChangedEvent e)
        {
            Device.BeginInvokeOnMainThread(() => UserStateChanged?.Invoke(this, e));
        }


    }
}
