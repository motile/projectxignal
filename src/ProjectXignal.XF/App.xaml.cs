﻿using ProjectXignal.Xf.ViewModels;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ProjectXignal.Xf
{

    public partial class App : Application
    {
        private readonly ISettingsService _settingsService;
        private readonly XignalManagerFactory _xignalManagerFactory;

        private SettingsModeContainer _settingsContainer;
        private XignalModeContainer _xignalContainer;


        public App(ISettingsService settingsService, XignalManagerFactory xignalManagerFactory)
        {
            InitializeComponent();

            _settingsService = settingsService;
            _xignalManagerFactory = xignalManagerFactory;

            var root = new Page(); // will be replaced when mode is set
            this.MainPage = new NavigationPage(root);

            this.SetMode();

            MessagingCenter.Subscribe<UserStateViewModel>(this, "EditSettings", async _ => await this.SetModeSettings());
        }

        private async Task SetModeSettings()
        {
            _xignalContainer?.Dispose();
            _xignalContainer = null;

            _settingsContainer = new SettingsModeContainer(_settingsService, SetMode);

            this.MainPage.Navigation.InsertPageBefore(_settingsContainer.View, this.MainPage.Navigation.NavigationStack[0]);
            await this.MainPage.Navigation.PopAsync();
        }

        private async Task SetModeXignal(string botToken, string accessToken)
        {
            _settingsContainer = null;

            var xignalManager = _xignalManagerFactory.Create(botToken, accessToken);

            _xignalContainer = new XignalModeContainer(xignalManager);

            this.MainPage.Navigation.InsertPageBefore(_xignalContainer.View, this.MainPage.Navigation.NavigationStack[0]);
            await this.MainPage.Navigation.PopAsync();
        }

        private async void SetMode()
        {
            var accessToken = await _settingsService.Get("accessToken");
            var botToken = await _settingsService.Get("botToken");

            if (string.IsNullOrEmpty(accessToken) || string.IsNullOrEmpty(botToken))
            {
                await SetModeSettings();

            }
            else
            {
                await SetModeXignal(botToken, accessToken);
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            _xignalContainer?.XignalManager?.Disconnect();
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            var t = _xignalContainer?.XignalManager?.Connect();
        }
    }
}
