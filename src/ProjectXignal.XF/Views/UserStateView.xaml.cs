﻿using ProjectXignal.Core;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectXignal.Xf.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserStateView : ContentPage
    {
        public UserStateView()
        {
            InitializeComponent();            
        }       
    }
}