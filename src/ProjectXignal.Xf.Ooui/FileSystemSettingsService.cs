﻿using System.Threading.Tasks;
using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ProjectXignal.Xf.Ooui
{
    public class FileSystemSettingsService : ISettingsService
    {
        private readonly string _file;

        public FileSystemSettingsService()
        {
            var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Xignal");

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            _file = Path.Combine(path, "settings.json");

            if (!File.Exists(_file))
            {
                using (var s = File.CreateText(_file))
                {
                    s.WriteLine("{ }");
                }
            }
        }
        public Task<string> Get(string key)
        {
            var jObject = JsonConvert.DeserializeObject<JObject>(File.ReadAllText(_file));

            string result = jObject[key]?.ToString();

            return Task.FromResult(result);
        }

        public Task Set(string key, string value)
        {
            var jObject = JsonConvert.DeserializeObject<JObject>(File.ReadAllText(_file));

            jObject[key] = value;

            File.WriteAllText(_file, JsonConvert.SerializeObject(jObject, Formatting.Indented));

            return Task.CompletedTask;
        }
    }

}
