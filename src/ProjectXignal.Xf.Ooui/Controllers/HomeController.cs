﻿using Microsoft.AspNetCore.Mvc;
using Ooui.AspNetCore;
using Xamarin.Forms;

namespace ProjectXignal.Xf.Ooui.Controllers
{
    public class HomeController : Controller
    {
        private readonly App _app;

        public HomeController(App app)
        {
            _app = app;
        }
        public IActionResult Index()
        {
            var element = _app.MainPage.GetOouiElement();

            return new ElementResult(element);
        }
    }
}
