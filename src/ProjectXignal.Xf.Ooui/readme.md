
# deploy to raspberry pi

### build
```
 dotnet publish -r win-arm -c release /p:TrimUnusedDependencies=true
```

### copy files to to pi

```
net use \\motilepi /USER:Administrator

mkdir \\motilepi\c$\Data\Users\Administrator\Documents\px

copy bin\Release\netcoreapp2.1\win-arm\publish \\motilepi\c$\Data\Users\Administrator\Documents\px

```

### Powershell-Remote-Session (run powershell as Administrator) 

```
Set-Item WSMan:\localhost\Client\TrustedHosts -Value motilepi

Enter-PSSession -ComputerName motilepi -Credential motilepi\Administrator

```
- more information: https://docs.microsoft.com/en-us/windows/iot-core/connect-your-device/powershell


### open firewall
```
netsh advfirewall firewall add rule name="Http Port 5000" dir=in action=allow protocol=TCP localport=5000
```

### run
```
  .\px\ProjectXignal.Xf.Ooui.exe --urls http://0.0.0.0:5000
```

### open in browser
```
explorer "http://motilepi:5000"
```
