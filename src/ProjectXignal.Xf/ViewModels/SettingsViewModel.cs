﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProjectXignal.Xf.ViewModels
{
    public class SettingsViewModel : INotifyPropertyChanged
    {
        private string _accessToken;
        private string _botToken;

        public string AccessToken
        {
            get
            {
                return _accessToken;
            }
            set
            {
                _accessToken = value;
                NotifyPropertyChanged();
            }
        }

        public string BotToken
        {
            get
            {
                return _botToken;
            }
            set
            {
                _botToken = value;
                NotifyPropertyChanged();
            }
        }
        public ICommand DoneCommand => new Command(_ => OnDone?.Invoke(this, EventArgs.Empty));


        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event EventHandler OnDone;
    }
}
