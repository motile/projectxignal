﻿using ProjectXignal.Core;
using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProjectXignal.Xf.ViewModels
{
    public class UserStateViewModel : INotifyPropertyChanged, IDisposable
    {

        private IXignalManager _xignalManager;

        public UserStateViewModel(IXignalManager xignalManager)
        {
            _xignalManager = xignalManager;
            var t = Init();
        }

        private async Task Init()
        {
            await _xignalManager.Connect();

            _xignalManager.UserStateChanged += _xignalManager_UserStateChanged;

            this.States = Enum.GetValues(typeof(State))
                .Cast<State>()
                .Where(x => x != State.Unknown)
                .ToArray();

         

            NotifyPropertyChanged(nameof(UserStates));
            NotifyPropertyChanged(nameof(States));
        }

        private void _xignalManager_UserStateChanged(object sender, UserStateChangedEvent e)
        {
            SetState(e.UserState);
        }

        public UserState[] UserStates => _xignalManager?.UserStates?
                .OrderBy(x => x.Name)
                .ToArray();

        public State[] States { get; private set; }

        public ICommand SetMyStateCommand => new Command((o) =>
        {
            if (o is State s)
            {
                SetMyState(s);
            }
        });

        public ICommand EditSettings => new Command((o) =>
        {
            MessagingCenter.Send<UserStateViewModel>(this, "EditSettings");
        });

        public event PropertyChangedEventHandler PropertyChanged;

        private void SetMyState(State state)
        {
            var myState = this.UserStates.Where(x => IsMyUser(x)).FirstOrDefault();
            if (myState == null)
                return;

            var newState = new UserState(myState.Id, myState.Name, state);

            _xignalManager?.SetState(newState);
        }

        private void SetState(UserState userState)
        {
            NotifyPropertyChanged(nameof(UserStates));
        }

        private bool IsMyUser(UserState state) => state != null && !string.IsNullOrEmpty(_xignalManager.UserId)
            && state.Id == _xignalManager.UserId;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Dispose()
        {
            if (_xignalManager != null)
            {
                _xignalManager.UserStateChanged -= _xignalManager_UserStateChanged;
                _xignalManager = null;
            }

        }
    }
}
