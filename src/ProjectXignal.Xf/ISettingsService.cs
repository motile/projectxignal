﻿using System.Threading.Tasks;

namespace ProjectXignal.Xf
{
    public interface ISettingsService
    {
        Task<string> Get(string key);
        Task Set(string key, string value);
    }
}
