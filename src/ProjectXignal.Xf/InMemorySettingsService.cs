﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectXignal.Xf
{
    public class InMemorySettingsService : ISettingsService
    {
        public Dictionary<string, string> _values = new Dictionary<string, string>()
        {
           

    };
        public Task<string> Get(string key) => Task.FromResult(_values.TryGetValue(key, out string value) ? value : string.Empty);


        public Task Set(string key, string value)
        {
            if (_values.ContainsKey(key))
                _values[key] = value;
            else
                _values.Add(key, value);

            return Task.CompletedTask;
        }
    }
}
