﻿using ProjectXignal.Core;
using ProjectXignal.Xf.ViewModels;
using ProjectXignal.Xf.Views;
using System;

namespace ProjectXignal.Xf
{
    public class XignalModeContainer : IDisposable
    {
        public XignalModeContainer(IXignalManager xignalManager)
        {
            XignalManager = xignalManager;
           
            this.ViewModel = new UserStateViewModel(XignalManager);
            this.View = new UserStateView() { BindingContext = this.ViewModel };
        }

        public IXignalManager XignalManager { get; }

        public UserStateViewModel ViewModel { get; }

        public UserStateView View { get; }

        public void Dispose()
        {
            XignalManager.Disconnect();

            if (XignalManager is IDisposable d)
            {
                d.Dispose();
            }
        }
    }
}
