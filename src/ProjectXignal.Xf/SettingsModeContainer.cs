﻿using ProjectXignal.Xf.ViewModels;
using ProjectXignal.Xf.Views;
using System;
using Xamarin.Forms.Xaml;


namespace ProjectXignal.Xf
{
    public class SettingsModeContainer
    {
        public SettingsModeContainer(ISettingsService settingsService, Action onDone)
        {
            this.Vm = new SettingsViewModel();
            this.View = new SettingsView() { BindingContext = Vm };

            var init = new Action(async () =>
            {
                this.Vm.AccessToken = await settingsService.Get("accessToken");
                this.Vm.BotToken = await settingsService.Get("botToken");
            });

            init();


            this.Vm.OnDone += async (s, e) =>
            {
                await settingsService.Set("accessToken", this.Vm.AccessToken);
                await settingsService.Set("botToken", this.Vm.BotToken);

                onDone?.Invoke();
            };

        }

        public SettingsViewModel Vm { get; }
        public SettingsView View { get; }




    }
}
