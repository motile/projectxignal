﻿using ProjectXignal.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace ProjectXignal.Xf.Converters
{
    public class UserState2ColorConverter : IValueConverter
    {
        public Color Convert(State s)
        {
            switch (s)
            {
                case State.Away:
                    return Color.Black;
                case State.Ready:
                    return Color.Green;
                default:
                    return Color.Orange;

            }
        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is UserState us)
                return Convert(us.State, targetType, parameter, culture);

            if (value is State s)
            {
                return Convert(s);
            }

            return Color.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
