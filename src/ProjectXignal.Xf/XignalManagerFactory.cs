﻿using ProjectXignal.Core;
using ProjectXignal.SlackNet;
using Xamarin.Forms.Xaml;


namespace ProjectXignal.Xf
{
    public class XignalManagerFactory
    {
        public IXignalManager Create(string botToken, string accessToken)
        {
            var xignalManager = new SlackXignalManager(botToken, accessToken);

            var wrapper = new UiThreadAwareXignalManagerWrapper(xignalManager);


            return wrapper;

        }
    }
}
