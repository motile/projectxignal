﻿using System.Threading.Tasks;
using Xamarin.Essentials;

namespace ProjectXignal.Xf
{
    public class SecureStorageSettingsService : ISettingsService
    {
        public Task<string> Get(string key) => SecureStorage.GetAsync(key);

        public Task Set(string key, string value) => SecureStorage.SetAsync(key, value);
    }
}
