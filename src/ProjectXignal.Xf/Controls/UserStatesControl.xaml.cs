﻿using ProjectXignal.Core;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Timers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectXignal.Xf.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserStatesControl : ContentView
    {
        private Timer _throttleAfterUserStateChanged;
        private Timer _throttleDraw;

        public UserStatesControl()
        {
            InitializeComponent();

            _throttleAfterUserStateChanged = new Timer
            {
                Interval = 250,
                AutoReset = false
            };
            _throttleAfterUserStateChanged.Elapsed += (s, e) => Xamarin.Forms.Device.BeginInvokeOnMainThread(() => AfterUserStateChanged?.Invoke(this, new EventArgs()));

            _throttleDraw = new Timer
            {
                Interval = 250,
                AutoReset = false
            };
            _throttleDraw.Elapsed += (s, e) => Xamarin.Forms.Device.BeginInvokeOnMainThread(Redraw);
        }

        public static readonly BindableProperty UserStatesProperty = BindableProperty.Create(
            propertyName: nameof(UserStatesControl.UserStates),
            returnType: typeof(IEnumerable<UserState>),
            declaringType: typeof(UserStatesControl),
            propertyChanged: UserStatesPropertyChanged,
            defaultValue: null
           );

        public IEnumerable<UserState> UserStates
        {
            get { return (IEnumerable<UserState>)GetValue(UserStatesProperty); }
            set { SetValue(UserStatesProperty, value); }
        }

        public event EventHandler AfterUserStateChanged;
             

        private static void UserStatesPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var c = ((UserStatesControl)bindable);

            if (oldValue is INotifyCollectionChanged ncOld)
                ncOld.CollectionChanged -= c.UserStates_CollectionChanged;

            if (oldValue is IEnumerable<UserState> oldList)
            {
                foreach (var entry in oldList)
                {
                    entry.PropertyChanged -= c.UserState_PropertyChanged;
                }
            }

            if (newValue is INotifyCollectionChanged ncNew)
                ncNew.CollectionChanged += c.UserStates_CollectionChanged;

            if (newValue is IEnumerable<UserState> newList)
            {
                foreach (var entry in newList)
                {
                    entry.PropertyChanged += c.UserState_PropertyChanged;
                }
            }

            c.TriggerRedraw();
            c.TriggerUserStateChanged();
        }

        private void UserStates_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            TriggerUserStateChanged();

            foreach (var entry in e.NewItems)
            {
                if (entry is INotifyPropertyChanged np)
                    np.PropertyChanged += UserState_PropertyChanged;
            }

            foreach (var entry in e.OldItems)
            {
                if (entry is INotifyPropertyChanged np)
                    np.PropertyChanged -= UserState_PropertyChanged;
            }


            TriggerRedraw();
        }

        private void UserState_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            TriggerUserStateChanged();
        }

        private void TriggerUserStateChanged()
        {
            _throttleAfterUserStateChanged.Start();
        }

        private void TriggerRedraw()
        {
            _throttleDraw.Start();
        }

        private void Redraw()
        {
            //vielleicht todo: statt clear gezielt Einträge hinzufügen, entfernen
            this.Users.Children.Clear();

            foreach (var u in this.UserStates)
                this.Users.Children.Add(this.CreateControlsForUser(u));
        }

        private View CreateControlsForUser(UserState user)
        {
            var r = new UserStateControl
            {
                UserState = user
            };

            return r;
        }
    }
}