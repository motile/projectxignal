﻿using ProjectXignal.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectXignal.Xf.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserStateControl : ContentView
    {
        public UserStateControl()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty UserStateProperty = BindableProperty.Create(
          propertyName: nameof(UserStateControl.UserState),
          returnType: typeof(UserState),
          declaringType: typeof(UserStateControl),
          defaultValue: null
         );

        public UserState UserState
        {
            get { return (UserState)GetValue(UserStateProperty); }
            set { SetValue(UserStateProperty, value); }
        }
    }
}