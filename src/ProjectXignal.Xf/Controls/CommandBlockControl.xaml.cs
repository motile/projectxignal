﻿using System.Collections;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectXignal.Xf.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CommandBlockControl : ContentView
    {
        public CommandBlockControl()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty ParametersProperty = BindableProperty.Create(nameof(Parameters), typeof(IEnumerable), typeof(CommandBlockControl), defaultValue: new object[0], propertyChanged: ParametersPropertyChanged);

        public static readonly BindableProperty CommandProperty = BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(CommandBlockControl));

        public IEnumerable Parameters
        {
            get { return (IEnumerable)GetValue(ParametersProperty); }
            set { SetValue(ParametersProperty, value); }
        }

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        private static void ParametersPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((CommandBlockControl)bindable).Redraw();
        }

        private void Redraw()
        {
            var states = this.Parameters?.Cast<object>().ToArray() ?? new object[0];

            var cmds = states.Select(x =>
            {
                var b = new Button
                {
                    CommandParameter = x,
                    Text = $"{x}"
                };

                b.SetBinding(Button.CommandProperty, new Binding() { Path = "SetMyStateCommand" });

                return b;
            }).ToArray();

            this.Commands.Children.Clear();

            foreach (var c in cmds)
                this.Commands.Children.Add(c);
        }


    }
}