﻿using System.Collections.Generic;
using System.Device.Cap1xxx;
using System.Device.I2c;

namespace System.Device
{
    public class TouchPhat : PimonoriBase<TouchPhatPad>
    {
        protected override IDictionary<TouchPhatPad, int> _pad2Led => new Dictionary<TouchPhatPad, int>
        {
            { TouchPhatPad.Back, 5 },
            { TouchPhatPad.A, 4 },
            { TouchPhatPad.B, 3 },
            { TouchPhatPad.C,2 },
            { TouchPhatPad.D, 1},
            { TouchPhatPad.Enter, 0}
        };

        private static TouchPhat _instance;

        public static TouchPhat Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new TouchPhat();
                }
                return _instance;
            }
        }

        protected override Cap1xxxLeds GetCap1Xxx()
        {
            var settings = new I2cConnectionSettings(1, 0x2c);

            var i2cDevice = I2cDevice.Create(settings);
            
            return new Cap1166(i2cDevice);
        }
    }
}
