﻿namespace System.Device
{
    public enum DrumHatPad
    {
        One = 3,
        Two = 2,
        Three = 1,
        Four = 0,
        Five = 6,
        Six = 5,
        Seven = 4,
        Eight = 7
    }
}
