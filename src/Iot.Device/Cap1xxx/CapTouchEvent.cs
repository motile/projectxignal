﻿using System;

namespace System.Device.Cap1xxx
{
    public class CapTouchEvent : EventArgs
    {
        public CapTouchEvent(byte channel, Status @event, byte delta)
        {
            Channel = channel;
            Event = @event;
            Delta = delta;

        }
        public byte Channel { get; }
        public Status Event { get; }
        public byte Delta { get; }
    }



}
