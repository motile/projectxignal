﻿using System.Device.I2c;

namespace System.Device.Cap1xxx
{
    public class Cap1208 : Cap1xxx
    {
        protected override byte[] Supported => new[] { PID_CAP1208 };

        public Cap1208(I2cDevice i2cDevice, int alertPin = -1, int resetPin = -1, bool skipInit = false)
           : base(i2cDevice, alertPin, resetPin, skipInit)
        {


        }
    }



}
