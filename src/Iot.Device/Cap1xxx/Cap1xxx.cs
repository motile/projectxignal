﻿using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Device.I2c;
using System.Linq;
using System.Threading;

namespace System.Device.Cap1xxx
{
    /// <summary>
    /// https://github.com/pimoroni/cap1xxx/blob/master/library/cap1xxx.py
    /// </summary>
    public partial class Cap1xxx : IDisposable
    {
        private readonly GpioController _gpioController;
        private readonly I2cDevice _i2cDevice;
        private readonly int _alertPin;
        private readonly int _resetPin;
        private readonly byte _productId;
        private byte _repeatEnabled = 0b00000000;
        private byte _releaseEnabled = 0b11111111;
        private byte _delta = 50;

        private readonly byte[] _inputDelta;
        private readonly bool[] _inputPressed;
        private readonly Status[] _inputStatus;

        private bool _isWatching = false;

        private AsyncWorker _asyncPoll;
        private object _i2cLock = new object();

        protected virtual byte[] Supported => new[] { PID_CAP1208, PID_CAP1188, PID_CAP1166 };
        protected virtual byte NumberOfInputs => 8;

        public Cap1xxx(I2cDevice i2cDevice, bool skipInit = false)
        {
            _i2cDevice = i2cDevice;

            _inputDelta = new byte[NumberOfInputs];
            _inputPressed = new bool[NumberOfInputs];
            _inputStatus = new Status[NumberOfInputs];

            _productId = GetProductId();

            if (!Supported.Contains(_productId))
            {
                throw new Exception($"Product Id {_productId} not supported!");
            }

            if (skipInit)
                return;

            // Enable all inputs with interrupt by default
            EnableInputs(0b11111111);
            EnableInterrupts(0b11111111);

            // Disable repeat for all channels, but give
            //it sane defaults anyway
            EnableRepeat(0b00000000);
            EnableMultitouch(true);

            SetHoldDelay(210);
            SetRepeatRate(210);

            // Tested sane defaults for various configurations
            WriteByte(R_SAMPLING_CONFIG, 0b00001000); // 1sample per measure, 1.28ms time, 35ms cycle
            WriteByte(R_SENSITIVITY, 0b01100000);     //  2x sensitivity

            WriteByte(R_GENERAL_CONFIG, 0b00111000);
            WriteByte(R_CONFIGURATION2, 0b01100000);

            SetTouchDelta(10);
        }

        public Cap1xxx(I2cDevice i2cDevice, int alertPin = -1, int resetPin = -1, bool skipInit = false)
            : this(i2cDevice, skipInit)
        {
            _gpioController = new GpioController(PinNumberingScheme.Logical);
            _alertPin = alertPin;
            _resetPin = resetPin;

            if (_alertPin != -1)
            {
                _gpioController.OpenPin(_alertPin, PinMode.InputPullUp);
            }

            if (_resetPin != -1)
            {
                _gpioController.OpenPin(_resetPin, PinMode.Output);
                _gpioController.Write(_resetPin, PinValue.High);
                Thread.Sleep(10);
                _gpioController.Write(_resetPin, PinValue.Low);
            }
        }

        private EventHandler<CapTouchEvent> _pressed;
        public event EventHandler<CapTouchEvent> Pressed
        {
            add
            {
                _pressed += value;
                StartWatching();
            }
            remove
            {
                _pressed -= value;
            }
        }

        private EventHandler<CapTouchEvent> _released;
        public event EventHandler<CapTouchEvent> Released
        {
            add
            {
                _released += value;
                StartWatching();
            }
            remove
            {
                _released -= value;
            }
        }

        protected byte ReadByte(byte register)
        {
            lock (_i2cLock)
            {
                _i2cDevice.WriteByte(register);
                return _i2cDevice.ReadByte();
            }
        }

        public void WriteByte(byte register, byte value)
        {
            lock (_i2cLock)
            {
                _i2cDevice.Write(new[] { register, value });
            }
        }

        protected byte[] ReadBlock(byte register, int length)
        {
            lock (_i2cLock)
            {
                var result = new byte[length];
                _i2cDevice.WriteByte(register);
                _i2cDevice.Read(result);
                return result;
            }
        }

        protected void EnableInputs(byte inputs) => WriteByte(R_INPUT_ENABLE, inputs);

        protected void EnableInterrupts(byte inputs) => WriteByte(R_INTERRUPT_EN, inputs);

        protected void EnableRepeat(byte inputs)
        {
            _repeatEnabled = inputs;
            WriteByte(R_REPEAT_EN, inputs);
        }

        /// <summary>
        /// Toggles multi-touch by toggling the multi-touch block bit in the config register
        /// </summary>        
        protected void EnableMultitouch(bool isEnabled = true)
        {
            var ret_mt = ReadByte(R_MTOUCH_CONFIG);
            var v = isEnabled ? ret_mt & ~0x80 : ret_mt | 0x80;
            WriteByte(R_MTOUCH_CONFIG, ret_mt);
        }

        protected byte GetProductId() => ReadByte(R_PRODUCT_ID);

        /// <summary>
        /// Set repeat rate in milliseconds, Clamps to multiples of 35 from 35 to 560
        /// </summary>
        /// <param name="ms"></param>
        protected void SetRepeatRate(int ms)
        {
            var repeat_rate = CalcTouchRate(ms);
            byte input_config = ReadByte(R_INPUT_CONFIG);
            input_config = unchecked((byte)((input_config & ~(0b1111)) | repeat_rate));
            WriteByte(R_INPUT_CONFIG, input_config);
        }

        protected int CalcTouchRate(int ms)
        {
            ms = Math.Min(Math.Max(ms, 0), 560);
            var scale = (int)((Math.Round(ms / 35.0, 0) * 35) - 35) / 35;
            return scale;
        }

        protected void SetTouchDelta(byte delta) => _delta = delta;

        public void Dispose()
        {
            StopWatching();

            if (_alertPin != -1)
            {
                _gpioController.ClosePin(_alertPin);
            }

            if (_resetPin != -1)
            {
                _gpioController.ClosePin(_resetPin);
            }
        }

        private bool StopWatching()
        {
            if (!_isWatching)
                return false;
            _isWatching = false;

            if (_alertPin != -1)
            {
                _gpioController.UnregisterCallbackForPinValueChangedEvent(_alertPin, OnPinValueChanged);
            }

            if (_asyncPoll != null)
            {
                _asyncPoll.Stop();
                _asyncPoll = null;
                return true;
            }
            return false;
        }

        public static byte GetTwosComp(byte val)
        {
            if ((val & (1 << (8 - 1))) != 0)
                val = (byte)(val - (1 << 8));

            return val;
        }

        /// <summary>
        /// Get the status of all inputs.
        //  Returns an array of 8 boolean values indicating
        //  whether an input has been triggered since the
        //  interrupt flag was last cleared.
        /// </summary>
        /// <returns></returns>
        private Status[] GetInputStatus()
        {
            var touched = ReadByte(R_INPUT_STATUS);
            var threshold = ReadBlock(R_INPUT_1_THRESH, NumberOfInputs);
            var delta = ReadBlock(R_INPUT_1_DELTA, NumberOfInputs);

            foreach (var x in Enumerable.Range(0, NumberOfInputs))
            {
                if (((1 << x) & touched) != 0)
                {
                    Status status = Status.none;
                    _delta = GetTwosComp(delta[x]);

                    //  # We only ever want to detect PRESS events
                    //  # If repeat is disabled, and release detect is enabled
                    if (_delta >= threshold[x])
                    {
                        _inputDelta[x] = _delta;
                        switch (_inputStatus[x])
                        {
                            case Status.press:
                            case Status.held:
                                if ((_repeatEnabled & (1 << x)) != 0)
                                {
                                    status = Status.held;
                                }
                                break;
                            case Status.none:
                            case Status.release:
                                if (_inputPressed[x])
                                    status = Status.none;
                                else
                                    status = Status.press;
                                break;
                        }
                    }
                    else
                    {
                        if ((_releaseEnabled & (1 << x)) != 0 && _inputStatus[x] != Status.release)
                            status = Status.release;
                        else
                            status = Status.none;

                    }

                    _inputStatus[x] = status;
                    _inputPressed[x] = (new[] { Status.press, Status.held, Status.none }).Contains(status);
                }
                else
                {
                    _inputStatus[x] = Status.none;
                    _inputPressed[x] = false;
                }
            }

            return _inputStatus;
        }


        /// <summary>
        /// Clear the interrupt flag, bit 0, of the main control register
        /// </summary>
        private void ClearInterupt()
        {
            var main = ReadByte(R_MAIN_CONTROL);
            main = unchecked((byte)(main & ~0b00000001));
            WriteByte(R_MAIN_CONTROL, main);
        }

        private bool InterruptStatus()
        {
            if (_alertPin == -1)
            {
                return (ReadByte(R_MAIN_CONTROL) & 1) == 1;
            }
            else
            {
                return _gpioController.Read(_alertPin) != PinValue.High;
            }
        }

        private long Millis() => (long)(DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds;


        /// <summary>
        /// Wait for, interrupt, bit 0 of the main
        /// control register to be set, indicating an
        /// input has been triggered.
        /// </summary>
        /// <param name="timeout"></param>
        private bool WaitForInterrupt(int timeout = 100)
        {
            var start = Millis();
            while (true)
            {
                var status = InterruptStatus();
                if (status)
                {
                    return true;
                }
                if (Millis() > start + timeout)
                {
                    return false;
                }
                Thread.Sleep(5);
            }
        }

        protected void SetBit(byte register, int bit)
        {
            var value = ReadByte(register);

            value |= (byte)(1 << bit);

            WriteByte(register, value);
        }

        protected void ClearBit(byte register, int bit)
        {
            var value = ReadByte(register);

            value &= (byte)~(1 << bit);

            WriteByte(register, value);
        }

        protected void ChangeBit(byte register, int bit, bool state)
        {
            if (state)
            {
                SetBit(register, bit);
            }
            else
            {
                ClearBit(register, bit);
            }
        }

        private void AutoRecalibrate(bool value)
        {
            ChangeBit(R_GENERAL_CONFIG, 3, value);
        }



        private void OnPinValueChanged(object source, PinValueChangedEventArgs args)
        {

            var inputs = GetInputStatus();
            ClearInterupt();

            foreach (var x in Enumerable.Range(0, NumberOfInputs))
            {
                TriggerHandler((byte)x, inputs[x]);
            }
        }

        private bool StartWatching()
        {
            if (_isWatching)
                return false;

            _isWatching = true;

            if (_alertPin != -1)
            {
                _gpioController.RegisterCallbackForPinValueChangedEvent(_alertPin, PinEventTypes.Falling, OnPinValueChanged);

                ClearInterupt();

                return true;
            }
            else
            {
                if (_asyncPoll == null)
                {
                    _asyncPoll = new AsyncWorker(Poll);
                    _asyncPoll.Start();
                    return true;
                }
                return false;

            }
        }

        /// <summary>
        /// Single polling pass, should be called in
        /// a loop, preferably threaded.
        /// </summary>
        private bool Poll()
        {
            if (WaitForInterrupt())
            {
                HandleAlert();
            }

            return true;
        }

        private void HandleAlert(int pin = -1)
        {
            Status[] inputs = GetInputStatus();
            ClearInterupt();
            foreach (var x in Enumerable.Range(0, NumberOfInputs))
            {
                TriggerHandler((byte)x, inputs[x]);
            }
        }

        private void TriggerHandler(byte channel, Status status)
        {
            switch (status)
            {
                case Status.press:
                    this._pressed?.Invoke(this, new CapTouchEvent(channel, status, _inputDelta[channel]));
                    break;
                case Status.release:
                    this._released?.Invoke(this, new CapTouchEvent(channel, status, _inputDelta[channel]));
                    break;
            }
        }

        public void FilterAnalogNoise(bool value) => ChangeBit(R_GENERAL_CONFIG, 4, !value);

        public void FilterDigitalNoise(bool value) => ChangeBit(R_GENERAL_CONFIG, 5, !value);

        /// <summary>
        /// Set time before a press and hold is detected,
        /// Clamps to multiples of 35 from 35 to 560
        /// </summary>
        /// <param name="ms"></param>
        public void SetHoldDelay(int ms)
        {
            var repeatRate = CalcTouchRate(ms);
            var inputConfig = ReadByte(R_INPUT_CONFIG2);
            inputConfig = (byte)((inputConfig & ~0b1111) | repeatRate);
            WriteByte(R_INPUT_CONFIG2, inputConfig);
        }



        protected void EnableInput(byte inputs) => WriteByte(R_INPUT_ENABLE, inputs);

        protected void ChangeBits(byte register, byte offset, byte size, byte bits)
        {
            var originalValue = ReadByte(register);
            foreach (var x in Enumerable.Range(0, size))
            {
                originalValue &= (byte)~(1 << (offset + x));
            }
            originalValue |= (byte)(bits << offset);
            WriteByte(register, originalValue);
        }
    }
}
