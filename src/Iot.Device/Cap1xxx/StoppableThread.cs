﻿using System.Linq;
using System.Threading;

namespace System.Device.Cap1xxx
{
    internal abstract class StoppableThread
    {
        private readonly Thread _thread;
        protected bool _stop = false;

        public StoppableThread()
        {
            _thread = new Thread(Run);
        }

        protected abstract void Run();

        public void Stop()
        {
            if (_thread.IsAlive)
            {
                _stop = true;
                _thread?.Join();
            }
        }

        public void Start()
        {
            if (!_thread.IsAlive)
            {
                _thread.Start();
            }
        }
    }



}
