﻿using System;

namespace System.Device.Cap1xxx
{
    internal class AsyncWorker : StoppableThread
    {
        private readonly Func<bool> _todo;

        public AsyncWorker(Func<bool> todo)
        {
            _todo = todo;
        }

        protected override void Run()
        {
            while (!_stop)
            {
                if (!_todo())
                {
                    _stop = true;
                    break;
                }
            }
        }
    }



}
