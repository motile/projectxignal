﻿using System.Device.I2c;

namespace System.Device.Cap1xxx
{
    public class Cap1188 : Cap1xxxLeds
    {
        protected override byte[] Supported => new[] { PID_CAP1188 };
        protected override byte NumberOfLeds => 8;

        public Cap1188(I2cDevice i2cDevice, int alertPin = -1, int resetPin = -1, bool skipInit = false)
           : base(i2cDevice, alertPin, resetPin, skipInit)
        {


        }
    }



}
