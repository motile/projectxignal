﻿using System.Device.I2c;

namespace System.Device.Cap1xxx
{
    public class Cap1166 : Cap1xxxLeds
    {
        protected override byte[] Supported => new[] { PID_CAP1166 };
        protected override byte NumberOfLeds => 6;
        protected override byte NumberOfInputs => 6;

        public Cap1166(I2cDevice i2cDevice, int alertPin = -1, int resetPin = -1, bool skipInit = false)
           : base(i2cDevice, alertPin, resetPin, skipInit)
        {


        }
    }



}
