﻿using System;
using System.Device.I2c;

namespace System.Device.Cap1xxx
{
    public class Cap1xxxLeds : Cap1xxx
    {
        protected virtual byte NumberOfLeds => 8;

        public Cap1xxxLeds(I2cDevice i2cDevice, int alertPin = -1, int resetPin = -1, bool skipInit = false)
            : base(i2cDevice, alertPin, resetPin, skipInit)
        {


        }

        public bool SetLedLinking(byte ledIndex, bool state)
        {
            if (ledIndex > NumberOfLeds)
                return false;

            ChangeBit(R_LED_LINKING, ledIndex, state);

            return true;
        }

        public bool SetLedOutputType(int ledIndex, bool state)
        {
            if (ledIndex > NumberOfLeds)
                return false;

            ChangeBit(R_LED_OUTPUT_TYPE, ledIndex, state);
            return true;
        }

        public bool SetLedState(int ledIndex, bool state)
        {
            if (ledIndex > NumberOfLeds)
                return false;

            ChangeBit(R_LED_OUTPUT_CON, ledIndex, state);
            return true;
        }

        public bool SetLedPolarity(int ledIndex, bool state)
        {
            if (ledIndex > NumberOfLeds)
                return false;

            ChangeBit(R_LED_POLARITY, ledIndex, state);
            return true;
        }

        /// <summary>
        /// Set the behaviour of a LED
        /// </summary>
        /// <param name="ledIndex"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool SetLedBehaviour(int ledIndex, byte value)
        {
            if (ledIndex > NumberOfLeds)
                return false;

            var offset = (byte)((ledIndex * 2) % 8);
            var register = (byte)(ledIndex / 4);
            value &= 0b00000011;

            ChangeBits((byte)(R_LED_BEHAVIOUR_1 + register), offset, 2, value);

            return true;
        }

        /// <summary>
        /// Set the overall period of a pulse from 32ms to 4.064 seconds
        /// </summary>        
        public void SetLedPulse1Period(double periodInSeconds)
        {
            periodInSeconds = Math.Min(periodInSeconds, 4.064);
            var value = (int)(periodInSeconds * 1000 / 32) & 0b01111111;
            ChangeBits(R_LED_PULSE_1_PER, 0, 7, (byte)value);
        }

        /// <summary>
        /// Set the overall period of a pulse from 32ms to 4.064 seconds
        /// </summary>        
        public void SetLedPulse2Period(double periodInSeconds)
        {
            periodInSeconds = Math.Min(periodInSeconds, 4.064);
            var value = (int)(periodInSeconds * 1000 / 32) & 0b01111111;
            ChangeBits(R_LED_PULSE_2_PER, 0, 7, (byte)value);
        }

        public void SetLedBreathePeriod(double periodInSeconds)
        {
            periodInSeconds = Math.Min(periodInSeconds, 4.064);
            var value = (int)(periodInSeconds * 1000 / 32) & 0b01111111;
            ChangeBits(R_LED_BREATHE_PER, 0, 7, (byte)value);
        }

        public void SetLedPulse1Count(byte count)
        {
            count -= 1;
            count &= 0b111;

            ChangeBits(R_LED_CONFIG, 0, 3, count);
        }

        public void SetLedPulse2Count(byte count)
        {
            count -= 1;
            count &= 0b111;

            ChangeBits(R_LED_CONFIG, 3, 3, count);
        }

        public void SetLedRampAlert(bool value)
        {
            ChangeBit(R_LED_CONFIG, 6, value);
        }

        /// <summary>
        /// Set the rise/fall rate in ms, max 2000.
        /// Rounds input to the nearest valid value.
        /// Valid values are 0, 250, 500, 750, 1000, 1250, 1500, 2000
        /// </summary>        
        public void SetLedDirectRampRate(int riseRate = 0, int fallRate = 0)
        {
            riseRate = (int)(Math.Round(riseRate / 250.0));
            fallRate = (int)(Math.Round(fallRate / 250.0));

            riseRate = Math.Min(7, riseRate);
            fallRate = Math.Min(7, fallRate);

            var rate = (byte)((riseRate << 4) | fallRate);
            WriteByte(R_LED_DIRECT_RAMP, rate);
        }

        public void SetLedDirectDuty(byte dutyMin, byte dutyMax)
        {
            var value = (byte)((dutyMin << 4) | dutyMax);
            WriteByte(R_LED_DIRECT_DUT, value);
        }

        public void SetLedPulse1Duty(byte dutyMin, byte dutyMax)
        {
            var value = (byte)((dutyMin << 4) | dutyMax);
            WriteByte(R_LED_PULSE_1_DUT, value);
        }

        public void SetLedPulse2Duty(byte dutyMin, byte dutyMax)
        {
            var value = (byte)((dutyMin << 4) | dutyMax);
            WriteByte(R_LED_PULSE_2_DUT, value);
        }

        public void SetLedBreatheDuty(byte dutyMin, byte dutyMax)
        {
            var value = (byte)((dutyMin << 4) | dutyMax);
            WriteByte(R_LED_BREATHE_DUT, value);
        }

        public void SetLedDirectMinDuty(byte value)
        {
            ChangeBits(R_LED_DIRECT_DUT, 0, 4, value);
        }

        public void SetLedDirectMaxDuty(byte value)
        {
            ChangeBits(R_LED_DIRECT_DUT, 4, 4, value);
        }

        public void SetLedBreatheMinDuty(byte value)
        {
            ChangeBits(R_LED_BREATHE_DUT, 0, 4, value);
        }

        public void SetLedBreatheMaxDuty(byte value)
        {
            ChangeBits(R_LED_BREATHE_DUT, 4, 4, value);
        }

        public void SetLedPulse1MinDuty(byte value)
        {
            ChangeBits(R_LED_PULSE_1_DUT, 0, 4, value);
        }

        public void SetLedPulse1MaxDuty(byte value)
        {
            ChangeBits(R_LED_PULSE_1_DUT, 4, 4, value);
        }

        public void SetLedPulse2MinDuty(byte value)
        {
            ChangeBits(R_LED_PULSE_2_DUT, 0, 4, value);
        }

        public void SetLedPulse2MaxDuty(byte value)
        {
            ChangeBits(R_LED_PULSE_2_DUT, 4, 4, value);
        }
    }



}
