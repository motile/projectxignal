﻿namespace System.Device.Cap1xxx
{

    public partial class Cap1xxx
    {

        // DEVICE MAP
        public static int DEFAULT_ADDR = 0x28;

        // Supported devices
        public static byte PID_CAP1208 = 0b01101011;
        public static byte PID_CAP1188 = 0b01010000;
        public static byte PID_CAP1166 = 0b01010001;

        // REGISTER MAP
        public static byte R_MAIN_CONTROL = 0x00;
        public static byte R_GENERAL_STATUS = 0x02;
        public static byte R_INPUT_STATUS = 0x03;
        public static byte R_LED_STATUS = 0x04;
        public static byte R_NOISE_FLAG_STATUS = 0x0A;

        // Read-only delta counts for all inputs
        public static byte R_INPUT_1_DELTA = 0x10;
        public static byte R_INPUT_2_DELTA = 0x11;
        public static byte R_INPUT_3_DELTA = 0x12;
        public static byte R_INPUT_4_DELTA = 0x13;
        public static byte R_INPUT_5_DELTA = 0x14;
        public static byte R_INPUT_6_DELTA = 0x15;
        public static byte R_INPUT_7_DELTA = 0x16;
        public static byte R_INPUT_8_DELTA = 0x17;

        public static byte R_SENSITIVITY = 0x1F;
        // B7     = N/A
        // B6..B4 = Sensitivity
        // B3..B0 = Base Shift
        //TODO: SENSITIVITY = {128: 0b000, 64:0b001, 32:0b010, 16:0b011, 8:0b100, 4:0b100, 2:0b110, 1:0b111}

        public static byte R_GENERAL_CONFIG = 0x20;
        // B7 = Timeout
        // B6 = Wake Config ( 1 = Wake pin asserted )
        // B5 = Disable Digital Noise ( 1 = Noise threshold disabled )
        // B4 = Disable Analog Noise ( 1 = Low frequency analog noise blocking disabled )
        // B3 = Max Duration Recalibration ( 1 =  Enable recalibration if touch is held longer than max duration )
        // B2..B0 = N/A

        public static byte R_INPUT_ENABLE = 0x21;


        public static byte R_INPUT_CONFIG = 0x22;

        public static byte R_INPUT_CONFIG2 = 0x23; // Default 0x00000111

        // Values for bits 3 to 0 of R_INPUT_CONFIG2
        // Determines minimum amount of time before
        // a "press and hold" event is detected.

        // Also - Values for bits 3 to 0 of R_INPUT_CONFIG
        // Determines rate at which interrupt will repeat

        // Resolution of 35ms, max = 35 + (35 * 0b1111) = 560ms

        public static byte R_SAMPLING_CONFIG = 0x24; // Default 0x00111001
        public static byte R_CALIBRATION = 0x26; // Default 0b00000000
        public static byte R_INTERRUPT_EN = 0x27;// Default 0b11111111
        public static byte R_REPEAT_EN = 0x28;// Default 0b11111111
        public static byte R_MTOUCH_CONFIG = 0x2A; // Default 0b11111111
        public static byte R_MTOUCH_PAT_CONF = 0x2B;
        public static byte R_MTOUCH_PATTERN = 0x2D;
        public static byte R_COUNT_O_LIMIT = 0x2E;
        public static byte R_RECALIBRATION = 0x2F;

        // R/W Touch detection thresholds for inputs
        public static byte R_INPUT_1_THRESH = 0x30;
        public static byte R_INPUT_2_THRESH = 0x31;
        public static byte R_INPUT_3_THRESH = 0x32;
        public static byte R_INPUT_4_THRESH = 0x33;
        public static byte R_INPUT_5_THRESH = 0x34;
        public static byte R_INPUT_6_THRESH = 0x35;
        public static byte R_INPUT_7_THRESH = 0x36;
        public static byte R_INPUT_8_THRESH = 0x37;

        // R/W Noise threshold for all inputs
        public static byte R_NOISE_THRESH = 0x38;

        // R/W Standby and Config Registers
        public static byte R_STANDBY_CHANNEL = 0x40;
        public static byte R_STANDBY_CONFIG = 0x41;
        public static byte R_STANDBY_SENS = 0x42;
        public static byte R_STANDBY_THRESH = 0x43;

        public static byte R_CONFIGURATION2 = 0x44;
        // B7 = Linked LED Transition Controls ( 1 = LED trigger is !touch )
        // B6 = Alert Polarity ( 1 = Active Low Open Drain, 0 = Active High Push Pull )
        // B5 = Reduce Power ( 1 = Do not power down between poll )
        // B4 = Link Polarity/Mirror bits ( 0 = Linked, 1 = Unlinked )
        // B3 = Show RF Noise ( 1 = Noise status registers only show RF, 0 = Both RF and EMI shown )
        // B2 = Disable RF Noise ( 1 = Disable RF noise filter )
        // B1..B0 = N/A

        // Read-only reference counts for sensor inputs
        public static byte R_INPUT_1_BCOUNT = 0x50;
        public static byte R_INPUT_2_BCOUNT = 0x51;
        public static byte R_INPUT_3_BCOUNT = 0x52;
        public static byte R_INPUT_4_BCOUNT = 0x53;
        public static byte R_INPUT_5_BCOUNT = 0x54;
        public static byte R_INPUT_6_BCOUNT = 0x55;
        public static byte R_INPUT_7_BCOUNT = 0x56;
        public static byte R_INPUT_8_BCOUNT = 0x57;

        // LED Controls - For CAP1188 and similar
        public static byte R_LED_OUTPUT_TYPE = 0x71;
        public static byte R_LED_LINKING = 0x72;
        public static byte R_LED_POLARITY = 0x73;
        public static byte R_LED_OUTPUT_CON = 0x74;
        public static byte R_LED_LTRANS_CON = 0x77;
        public static byte R_LED_MIRROR_CON = 0x79;

        // LED Behaviour
        public static byte R_LED_BEHAVIOUR_1 = 0x81;// For LEDs 1-4
        public static byte R_LED_BEHAVIOUR_2 = 0x82; // For LEDs 5-8
        public static byte R_LED_PULSE_1_PER = 0x84;
        public static byte R_LED_PULSE_2_PER = 0x85;
        public static byte R_LED_BREATHE_PER = 0x86;
        public static byte R_LED_CONFIG = 0x88;
        public static byte R_LED_PULSE_1_DUT = 0x90;
        public static byte R_LED_PULSE_2_DUT = 0x91;
        public static byte R_LED_BREATHE_DUT = 0x92;
        public static byte R_LED_DIRECT_DUT = 0x93;
        public static byte R_LED_DIRECT_RAMP = 0x94;
        public static byte R_LED_OFF_DELAY = 0x95;

        // R/W Power buttonc ontrol
        public static byte R_POWER_BUTTON = 0x60;
        public static byte R_POW_BUTTON_CONF = 0x61;

        // Read-only upper 8-bit calibration values for sensors
        public static byte R_INPUT_1_CALIB = 0xB1;
        public static byte R_INPUT_2_CALIB = 0xB2;
        public static byte R_INPUT_3_CALIB = 0xB3;
        public static byte R_INPUT_4_CALIB = 0xB4;
        public static byte R_INPUT_5_CALIB = 0xB5;
        public static byte R_INPUT_6_CALIB = 0xB6;
        public static byte R_INPUT_7_CALIB = 0xB7;
        public static byte R_INPUT_8_CALIB = 0xB8;

        // Read-only 2 LSBs for each sensor input
        public static byte R_INPUT_CAL_LSB1 = 0xB9;
        public static byte R_INPUT_CAL_LSB2 = 0xBA;

        // Product ID Registers
        public static byte R_PRODUCT_ID = 0xFD;
        public static byte R_MANUFACTURER_ID = 0xFE;
        public static byte R_REVISION = 0xFF;

        // LED Behaviour settings
        public static byte LED_BEHAVIOUR_DIRECT = 0b00;
        public static byte LED_BEHAVIOUR_PULSE1 = 0b01;
        public static byte LED_BEHAVIOUR_PULSE2 = 0b10;
        public static byte LED_BEHAVIOUR_BREATHE = 0b11;

        // ?? LED_OPEN_DRAIN = 0; // Default, LED is open-drain output with ext pullup
        // ?? LED_PUSH_PULL = 1; // LED is driven HIGH/LOW with logic 1/0

        // ?? LED_RAMP_RATE_2000MS = 7;
        // ?? LED_RAMP_RATE_1500MS = 6;
        // ?? LED_RAMP_RATE_1250MS = 5;
        // ?? LED_RAMP_RATE_1000MS = 4;
        // ?? LED_RAMP_RATE_750MS = 3;
        // ?? LED_RAMP_RATE_500MS = 2;
        // ?? LED_RAMP_RATE_250MS = 1;
        // ?? LED_RAMP_RATE_0MS = 0;


    }

}

