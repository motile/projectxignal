﻿using System.Collections.Generic;
using System.Device.Cap1xxx;
using System.Linq;

namespace System.Device
{
    public abstract class PimonoriBase<T> where T : Enum
    {
        public static readonly T[] Pads = Enum.GetValues(typeof(T)).Cast<T>().ToArray();

        protected abstract IDictionary<T, int> _pad2Led { get; }

        private readonly Cap1xxxLeds _cap1xxx;

        private bool _autoLeds = true;

        protected PimonoriBase()
        {
            _cap1xxx = GetCap1Xxx();
            _cap1xxx.Pressed += HandlePress;
            _cap1xxx.Released += HandleRelease;

            //"""Unlink the LEDs since Touch pHAT"s LEDs don"t match up with the channels"""
            _cap1xxx.WriteByte(Cap1xxx.Cap1xxx.R_LED_LINKING, 0b00000000);
        }

        public event EventHandler<PimonoriTouchEvent<T>> Pressed;
        public event EventHandler<PimonoriTouchEvent<T>> Released;

        protected abstract Cap1xxxLeds GetCap1Xxx();

        public void LedOn(T pad) => _cap1xxx.SetLedState(Pad2Led(pad), true);
        public void LedOff(T pad) => _cap1xxx.SetLedState(Pad2Led(pad), false);

        private void HandlePress(object sender, CapTouchEvent @event)
        {
            var pad = Channel2Pad(@event.Channel);

            if (_autoLeds)
                _cap1xxx.SetLedState(Pad2Led(pad), true);

            Pressed?.Invoke(this, new PimonoriTouchEvent<T>(Channel2Pad(@event.Channel)));
        }

        private void HandleRelease(object sender, CapTouchEvent @event)
        {
            var pad = Channel2Pad(@event.Channel);

            if (_autoLeds)
                _cap1xxx.SetLedState(Pad2Led(pad), false);

            Released?.Invoke(this, new PimonoriTouchEvent<T>(Channel2Pad(@event.Channel)));
        }

        private int Pad2Led(T pad) => _pad2Led[pad];

        public void SetLed(T pad, bool value) => _cap1xxx.SetLedState(Pad2Led(pad), value);

        protected T Channel2Pad(int channel)
        {
            return Pads.Where(x => Convert.ToInt32(x) == channel).FirstOrDefault();
        }

        public void AllOn()
        {
            foreach (var pad in Pads)
            {
                _cap1xxx.SetLedState(Pad2Led(pad), true);
            }
        }

        public void AllOff()
        {
            foreach (var pad in Pads)
            {
                _cap1xxx.SetLedState(Pad2Led(pad), false);
            }
        }

    }
}
