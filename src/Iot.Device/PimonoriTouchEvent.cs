﻿using System;

namespace System.Device
{
    public class PimonoriTouchEvent<T> : EventArgs
        where T : Enum
    {
        public PimonoriTouchEvent(T pad)
        {
            Pad = pad;
        }

        public T Pad { get; }
    }
}
