﻿using System.Collections.Generic;
using System.Device.Cap1xxx;
using System.Device.I2c;

namespace System.Device
{
    public class DrumHat : PimonoriBase<DrumHatPad>
    {
        protected override IDictionary<DrumHatPad, int> _pad2Led => new Dictionary<DrumHatPad, int>
        {
            { DrumHatPad.One, 2 },
            { DrumHatPad.Two, 3 },
            { DrumHatPad.Three, 4 },
            { DrumHatPad.Four, 5 },
            { DrumHatPad.Five, 6 },
            { DrumHatPad.Six, 0 },
            { DrumHatPad.Seven, 1 },
            { DrumHatPad.Eight, 7 }
        };

        private static DrumHat _instance;

        public static DrumHat Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DrumHat();
                    _instance.AllOff();
                }
                return _instance;
            }
        }

        protected override Cap1xxxLeds GetCap1Xxx()
        {
            var settings = new I2cConnectionSettings(1, 0x2c);

            var i2cDevice = I2cDevice.Create(settings);

            return new Cap1188(i2cDevice, alertPin: 25);
        }
    }
}
