﻿using System;

namespace ProjectXignal.Core
{
    public class UserStateChangedEvent : EventArgs
    {
        public UserState UserState { get; set; }
    }
}
