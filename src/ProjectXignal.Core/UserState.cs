﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ProjectXignal.Core
{
    public class UserState : IEquatable<UserState>, INotifyPropertyChanged
    {
        private State _state;

        public UserState(string id, string name, State state)
        {
            this.Id = id;
            this.Name = name;
            this._state = state;
        }

        public string Id { get; }
        public string Name { get; }
        public State State
        {
            get { return _state; }
            set
            {
                this._state = value;
                NotifyPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool Equals(UserState other) => (Id, Name, State) == (other.Id, other.Name, other.State);

        public override bool Equals(object obj) => (obj is UserState userState) && Equals(userState);

        public override int GetHashCode() => (Id, Name, State).GetHashCode();
    }
}
