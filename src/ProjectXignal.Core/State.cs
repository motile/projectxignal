﻿namespace ProjectXignal.Core
{
    public enum State
    {
        Unknown,
        Ready,
        Away,
        Busy
    }
}
