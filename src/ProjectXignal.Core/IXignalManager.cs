﻿using System;
using System.Threading.Tasks;

namespace ProjectXignal.Core
{
    public interface IXignalManager
    {
        string User { get; }

        string UserId { get; }

        UserState[] UserStates { get; }

        bool IsConnected { get; }

        Task Connect();

        void Disconnect();

        event EventHandler<UserStateChangedEvent> UserStateChanged;

        void SetState(UserState state);
    }
}