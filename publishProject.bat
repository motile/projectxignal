pushd src\ProjectXignal.Xf.Ooui

dotnet build -r linux-arm  

pushd bin\Debug\netcoreapp2.2\linux-arm
del lib*.*
del publish /S /Q
pscp -v -pw motile  -r .\* pi@motilepi:/home/pi/ProjectXignal

popd
popd